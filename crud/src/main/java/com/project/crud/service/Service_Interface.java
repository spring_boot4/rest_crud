package com.project.crud.service;

import java.util.List;

import com.project.crud.entity.VloudVendor;

public interface Service_Interface {
	
public String createVendor(VloudVendor vloudVendor);
public String updateVendor(VloudVendor vloudVendor);
public String deleteVendor(int id);
public VloudVendor getVendor(int id);
public List<VloudVendor>getallVendor();
}
