package com.project.crud.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.project.crud.entity.VloudVendor;
import com.project.crud.repository.VendorRepo;

@Service
public class VendorService implements Service_Interface{

	private VendorRepo repo;
	
	

	public VendorService(VendorRepo repo) {
		
		this.repo = repo;
	}

	@Override
	public String createVendor(VloudVendor vloudVendor) {
		repo.save(vloudVendor);
		
		return "saved";
	}

	@Override
	public String updateVendor(VloudVendor vloudVendor) {
		repo.save(vloudVendor);
		return "updated";
	}

	@Override
	public String deleteVendor(int id) {
		repo.deleteById(id);
		return "Deleted";
	}

	@Override
	public VloudVendor getVendor(int id) {
		
		return repo.findById(id).get();
	}

	@Override
	public List<VloudVendor> getallVendor() {
		
		return repo.findAll();
	}

	
}
