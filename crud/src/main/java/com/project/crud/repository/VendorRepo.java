package com.project.crud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project.crud.entity.VloudVendor;

public interface VendorRepo extends JpaRepository<VloudVendor,Integer>{

}
