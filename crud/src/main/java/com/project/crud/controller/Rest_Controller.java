package com.project.crud.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.crud.entity.VloudVendor;
import com.project.crud.service.Service_Interface;

@RestController
@RequestMapping("/vendor")
public class Rest_Controller {

	@Autowired
	private Service_Interface service;

	public Rest_Controller(Service_Interface service) {
		
		this.service = service;
	}
	

@GetMapping("{Ids}")
public VloudVendor getoneVendor(@PathVariable("Ids") int id) {
		
		return service.getVendor(id);
	}

@GetMapping()
public List<VloudVendor> getallVendors() {
		
		return service.getallVendor();
	}


@PostMapping
public String createvendor(@RequestBody VloudVendor vloudVendor) {
	service.createVendor(vloudVendor);
	
	return "saved successfully";
}


@PutMapping
public String updateVendors(@RequestBody VloudVendor vloudVendor) {
	service.updateVendor(vloudVendor);
	return "updated successfully";
}

@DeleteMapping("{vId}")
public String deletevendor(@PathVariable("vId") int id) {
	service.deleteVendor(id);
	return "Deleted";
}
}
